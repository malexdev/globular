var debug = require('debug')('globular');

debug('Current directory: %s', process.cwd());
var Project = require('./lib/project');

function main() {
    var root = new Project(process.cwd());

    if (!root.isValid()) {
        exit('Current folder is not a Node.js project');
    }

    root.install();
}

function exit() {
    console.log.apply(this, arguments);
    process.exit();
}

main();
