var debug = require('debug')('globular:lib:package');

var path = require('path');
var _ = require('lodash');
var util = require('util');
var fs = require('fs-extra');
var osenv = require('osenv');

var globalDir = path.join(osenv.home(), '.globular', 'modules');

function Project(root) {

    this.path = {
        root,
        package: path.join(root, 'package.json'),
        dependencies: path.join(root, 'node_modules')
    };

    this.package = {};
    this.name = '';
    this.version = '';
    this.dependencies = [];

    debug('Created new project object at path: %s', this.path.root);

    this.load();
}

Project.prototype.toString = function() {
    return util.format('%s v%s', this.name, this.version);
};

Project.prototype.isValid = function () {
    return fs.existsSync(this.path.package);
};

Project.prototype.load = function() {
    var data = fs.readJsonSync(this.path.package);

    this.package = data;
    this.name = data.name;
    this.version = data.version;

    this.loadDependencies();
    return this;
};

Project.prototype.loadDependencies = function() {
    this.dependencies =
        (fs.existsSync(this.path.dependencies) ? listNodeDependencies(this.path.dependencies) : [])
        .map(function(p) {
            return new Project(path.join(this.path.dependencies, p));
        }, this);
    return this;
}

function listNodeDependencies(dir) {
    var candidates = fs.readdirSync(dir);
    return candidates.filter(function(candidate) {
        return fs.existsSync(path.join(dir, candidate, 'package.json'))
    });
}

Project.prototype.listDeps = function(callback) {
    return this.dependencies.map(function(p) { return p.name; });
};

Project.prototype.rootPathForDep = function(name) {
    var npm3path = path.join(process.cwd(), name);
    var npm2path = path.join(this.path.dependencies, name);

    return fs.existsSync(npm3path) ? npm3path : npm2path;
};

Project.prototype.globalPath = function() {
    return path.join(globalDir, this.name, this.version);
};

Project.prototype.install = function(recursive) {
    var installPath = this.globalPath();

    debug('Installing %s in global path: %s', this.name, installPath);
    fs.mkdirsSync(installPath);
    fs.emptydirSync(installPath);

    fs.copySync(this.path.root, installPath, {filter: /^(?!node_modules).*$/});

    if (recursive) {
        this.dependencies.forEach(function(dep) {
            dep.install(recursive);
        });
    }
};

module.exports = Project;
